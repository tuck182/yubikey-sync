use clap::Parser;
use serde::Deserialize;
use std::collections::HashSet;
use std::process::Command;

/// Read TOTP details from 1Password and update attached Yubikey
#[derive(Parser)]
struct Cli {
    /// Title of 1Password notes entry containing TOTP codes in YAML
    #[clap(short = 't', long, default_value = "TOTP 2FA Codes")]
    notes_title: String,
    /// Device ID/Serial of Yubikey (run `ykman list` to get id)
    device: String,
    /// Don't update key, just print what would be changed
    #[clap(short = 'n', long)]
    dry_run: bool,
}


#[derive(Debug, PartialEq, Eq, Hash)]
struct OathAccount {
    issuer: String,
    name: String,
}

#[derive(Debug, Deserialize)]
struct OathAccountConfig {
    issuer: String,
    name: String,
    secret: String,
    #[allow(dead_code)]
    url: Option<String>,
}

fn get_accounts_config(title: &str) -> Vec<OathAccountConfig> {
    println!("Invoking 1password-cli; check 1Password app for authorization");

    let output = String::from_utf8(
        Command::new("op")
        .arg("item")
        .arg("get")
        .arg(title)
        .arg("--fields")
        .arg("notesPlain")
        .arg("--format")
        .arg("json")
        .output()
        .expect("op command failed").stdout)
        .expect("Failed to parse op output as string");

    let parsed = json::parse(output.as_str()).expect("Failed to parse op output as json");
    let notes = parsed["value"].as_str().expect("Failed to extract notesPlain value");
    let entries: Vec<OathAccountConfig> = serde_yaml::from_str(notes).expect("Failed to parse 2FA codes");

    return entries;
}

fn get_current_accounts(device_id: &str) -> HashSet<OathAccount> {
    let output = String::from_utf8(
        Command::new("sudo")
        .arg("ykman")
        .arg("--device")
        .arg(device_id)
        .arg("oath")
        .arg("accounts")
        .arg("list")
        .output()
        .expect("ykman command failed").stdout)
        .expect("Failed to parse ykman output as string");

    let accounts: HashSet<OathAccount> = output.split("\n")
        .into_iter()
        .filter_map(|line| -> Option<OathAccount> {
            if let Some((issuer, name)) = line.split_once(":") {
                Some(OathAccount {
                    issuer: issuer.to_string(),
                    name: name.to_string(),
                })
            } else {
                None
            }
        }).collect();
    accounts
}

fn add_account(device_id: &str, account: &OathAccountConfig) {
    String::from_utf8(
            Command::new("sudo")
            .arg("ykman")
            .arg("--device")
            .arg(device_id)
            .arg("oath")
            .arg("accounts")
            .arg("add")
            .arg("-f")
            .arg("-i")
            .arg(&account.issuer)
            .arg(&account.name)
            .arg(&account.secret)
            .output()
            .expect("ykman command failed").stdout)
            .expect("Failed to parse ykman output as string");
}

fn main() {
    let args: Cli = Cli::parse();
    let device_id = args.device.as_str();

    let current_accounts  = get_current_accounts(device_id);
    let entries = get_accounts_config(args.notes_title.as_str());
    for entry in &entries {
        let key = OathAccount{issuer: entry.issuer.clone(), name: entry.name.clone()};
        if current_accounts.contains(&key) {
            println!("Updating account ({}:{})", entry.issuer, entry.name);
        } else {
            println!("Adding account ({}:{})", entry.issuer, entry.name);
        }
        if !args.dry_run {
            add_account(device_id, entry);
        }
    }
}
